BEGIN{
    print "digraph G {"
    print "rankdir=LR;"
    print "size="200""
    print "node [shape=box]"
    
}
{
    filas[NR]
    cambio_varialbe=$3$2
    cambio_aminoacido=$4$6
    d="\""
    print cambio_varialbe " [label = " d$3d "]"
    print cambio_aminoacido "[label = " d$4d "]"
    print cambio_varialbe "->" cambio_aminoacido
	auxiliar=$4$6
	for (i=0; i<NR; i++){
	    if (NR == 1){
	        p=$4$6
	    }    
        if (p != auxiliar){
            print "node [shape=box]"
            print p "->" auxiliar "[label =  "d"Cadena"" "$5d"];"
            p=$4$6
    
        }
    }        
}
END{

    print "}"
}


