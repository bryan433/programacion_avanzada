Titulo del proyecto: Filtro de proteinas y visualizacion

Objetivo: este proyecto consiste en trabajar con bash y a traves de una una base de datos que contiene proteinas y varias cosas mas, debemos filtrar las que sean solamente proteinas, luego al extraer estos datos debemos graficarla.

Instalacion: Para poder ejecutar este programa se tiene que instalar o tener instalado ubuntu, cualquier version sirve para poder ejecutarlo, luego al hacer el primer paso debemos dirijirnos hacia el respositorio https://gitlab.com/bryan433/programacion_avanzada, entrar a la carpeta llamada proyecto 1 copiar el link, ejecutar la terminal y escribir wget el link de la pagina, abrir la terminal desde la carpeta donde tengan alojado el programa, despues en la terminal escriben bash nombre_del_archivo.sh, y estara listo su programa.

Versiones:
	GNU bash, versión 4.4.19(1)-release (x86_64-pc-linux-gnu)
	Copyright (C) 2016 Free Software Foundation, Inc.
	Licencia GPLv3+: GPL de GNU versión 3 o posterior 		<http://gnu.org/licenses/gpl.html>

	Esto es software libre; siéntase libre para cambiarlo y 	redistribuirlo.
	No existe NINGUNA GARANTÍA, en la medida permitida por la ley.

Autor:
	Bryan Ahumada.
	

