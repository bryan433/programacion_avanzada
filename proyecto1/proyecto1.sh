#!/bin/bash -x

NOMBRE_PROTEINA=$1

function recorrer_fichero(){
	if [[ $NOMBRE_PROTEINA = "" ]]; then
	    echo "Debe ingresar alguna proteina como parametro,"
	    echo "intentelo nuevamente ejecutando el programa"
	    exit 1
	else     
        while read linea; do
	        echo $linea
        done < bd-pdb.txt | grep \"$NOMBRE_PROTEINA\" | grep \"Protein\"
    fi	
}

function verificar_proteina(){
    recorrer_fichero    
    m=`cat bd-pdb.txt | grep \"$NOMBRE_PROTEINA\" | awk {"print $4"}`
    n=`cat bd-pdb.txt | grep \"$NOMBRE_PROTEINA\" | grep \"Protein\"`
    if [[ $n = $m ]]; then
        echo
        echo "La proteina que buscaba, se encuentra en nuestra base de datos"
        echo
        echo "--------------------------------------------------------------------------------"
        comprobar_conexion
    else
        echo
        echo "La proteina no esta en la base de datos, o no es una proteina"
        echo 
    fi    
}

function comprobar_conexion(){
    if ping -c 1 google.com; then
       echo "Usted tiene conexion de internet, podremos descargar el archivo"
       descargar_proteina
    else
        echo "Usted no tiene conexion de internet, conecte su PC, para descargar el archivo" 
    fi
}

function descargar_proteina(){
    if [[ ! -f $NOMBRE_PROTEINA.pdb ]]; then
        echo 
        echo "--------------------------------------------------------------------------------"
        echo "No existe el archivo, lo descargaremos"
        echo 
        wget https://files.rcsb.org/download/$NOMBRE_PROTEINA.pdb 
        cat $NOMBRE_PROTEINA.pdb | grep ^ATOM | awk 'length($3) > 3{$3 = substr($3,1,3)" "substr($3,5,3)}length($4) > 3{$4 = substr($4,2,3)}{print $0}' > $NOMBRE_PROTEINA.txt
        cat $NOMBRE_PROTEINA.txt | awk -f graficar.awk >> $NOMBRE_PROTEINA.dot
        echo
        echo "--------------------------------------------------------------------------------"
        echo "Vamos a graficar su proteina: "
        dot -Tps -o $NOMBRE_PROTEINA.ps $NOMBRE_PROTEINA.dot
        echo "El grafico se encuentra en la carpeta del proyecto"
        
    else
        echo "ya esta descargado el archivo"
        cat $NOMBRE_PROTEINA.txt
        fi	
}


verificar_proteina
