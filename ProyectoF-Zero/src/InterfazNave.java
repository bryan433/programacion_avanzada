
public interface InterfazNave {
	
	public Odometro getVelocimetro();
	
	public void setVelocimetro(Odometro velocimetro);
	
	public int getPatente();

	public void setPatente(int patente);
	
	public int getDurabilidad();

	public void setDurabilidad(int durabilidad);
	
	public boolean isInterruptor();
	
	public void setInterruptor(boolean interruptor);
	
	public int getTiempo();

	public void setTiempo(int tiempo);
	
	public int getAceleracion();

	public void setAceleracion(int aceleracion);
	
	public String getNombre();
	
	public void setNombre(String nombre);
	
    public int compareTo(InterfazNave o);

}
