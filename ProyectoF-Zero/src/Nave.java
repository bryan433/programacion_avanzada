
public class Nave implements Comparable<InterfazNave>, InterfazNave   {	
	// Composición genérica.
	Motor Motor;
	Estanque Estanque;
	private Odometro Velocimetro;
	Turbina Turbina;
	private int aceleracion;
	private int tiempo;
	private boolean interruptor;
	private String Nombre;
	private int patente;	
	private int durabilidad;
	
	// Constructor.
	Nave(){
		aceleracion = 0;
		interruptor = false;
		Motor = new Motor();
		Estanque = new Estanque();
		Velocimetro = new Odometro();
		Turbina = new Turbina();
		durabilidad = 100;
	}	
	
	public Odometro getVelocimetro() {
		return Velocimetro;
	}


	public void setVelocimetro(Odometro velocimetro) {
		Velocimetro = velocimetro;
	}


	public int getPatente() {
		return patente;
	} 

	public void setPatente(int patente) {
		this.patente = patente;
	}
	
	public int getDurabilidad() {
		return durabilidad;
	}

	public void setDurabilidad(int durabilidad) {
		this.durabilidad = durabilidad;
	}
	
	public boolean isInterruptor() {
		return interruptor;
	}

	public void setInterruptor(boolean interruptor) {
		this.interruptor = interruptor;
	}
	
	public int getTiempo() {
		return tiempo;
	}

	public void setTiempo(int tiempo) {
		this.tiempo = tiempo;
	}
	
	public int getAceleracion() {
		return aceleracion;
	}

	public void setAceleracion(int aceleracion) {
		this.aceleracion = aceleracion;
	}
	
	public String getNombre() {
		return Nombre;
	}
	
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	
    public int compareTo(InterfazNave o) {
        int resultado = 0;
        if(this.tiempo>o.getTiempo()){
        	resultado =+1;
        }
        else if (this.tiempo<o.getTiempo()){
        	resultado =- 1;
        }
        return resultado;
    }
}