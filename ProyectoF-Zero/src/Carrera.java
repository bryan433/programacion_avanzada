import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;
import java.util.Random;

public class Carrera {
	private ArrayList<Nave> NaveRobot;
	boolean zapatilla;
	int participantes;
	int columna;
	int fila;
	Scanner sc;
	Nave naveUsuario;
	String tablero[][];
	String nombre;
	Mecanico Mecanico;
	Locutor Locutor;
	 
	Carrera(){
		NaveRobot = new ArrayList<Nave>();
		Mecanico = new Mecanico();
		sc = new Scanner(System.in);
		naveUsuario = new Nave();
		nombre = " ";
		columna = 0;
		fila = 0;
		zapatilla = true;
		participantes = 3;
		System.out.println("Bienvenidos al juego F-Zero");
	}
	
	public void locutor(int tiempo, int fila, Nave i, int contador) {
			if ( i.getDurabilidad() < 10) {
				System.out.println("La nave del carril " + i.getPatente() + " se ve que comienza a fallar");
			}
			else if (i.isInterruptor() == true ) {		
				if ( contador  == 1) {
					System.out.println("Ya tenemos un ganador, el del carril "+i.getPatente() );
				}
			}
			else if (i.getTiempo() == 0 && contador == 0) {
				System.out.print("Empezoooo!!!");
				
			}
			else if ( (i.getTiempo() == 2) && (i.getVelocimetro().getDistancia() <= 2)) {
				System.out.println("El tamaño de la pista es de:  "+ fila );
			}
			
			else if ( i.getVelocimetro().getDistancia() == fila - 1) {
				System.out.println("La nave " + i.getNombre()+" del carril "+i.getPatente()+" ya llegó a la meta.");
			}
			
			else if ( i.getDurabilidad() == 100 && tiempo != 1) {
				System.out.println("La del carril "+ i.getPatente()+" quedó como nueva!!.");
				if (i.getNombre() == "#") {
					System.out.println("Arreglar las naves con combustible nuclear, son un puro problema");
				}
				else if (i.getNombre() == "X") {
					System.out.println("Las naves con combustible diesel, rinden ahí noma");
				}
				else {
					System.out.println("Le hecharé combustible BioDiesel a mi nave ");
				}
				System.out.println(" Pero que gran mecánico");
			}
			else if (i.getTiempo() > 20) {
				System.out.println("La carrera esta bastante pareja.");
			}
			else if (i.getTiempo() == 22) {
				System.out.println("La nave del carril"+i.getPatente()+" deberia ahorrar para comprarse otra nave");
			}
			else if (tiempo == 4 && contador == 0) {
				System.out.println(" Van avanzando a buena velocidad");
			}
			else {
				Locutor = new Locutor();
				if (Locutor.getRandom() == 1 && i.getTiempo() != 0) {
					System.out.println("Un piloto se sale y queda atrapado en el fango,");
					System.out.println("quita el volante para bajarse del coche y el locutor comenta:");
					System.out.println("Qué enfadado está el piloto!!! Ha arrancado el volante de la nave!!!");
				 }
				 else if (Locutor.getRandom() == 2 && i.getTiempo() != 0) {
					 System.out.println("¿Qué combustible ocuparán las naves?");
					 System.out.println("Repasemoslo!");
					 System.out.println("|Combustible|"+"   Nombre  |"); 
					 for (Nave z: NaveRobot) {
						 if (z.getNombre() == "#") {
							 System.out.println("|Nuclear    |    "+z.getNombre()+"     |"); 
						 }
						 if (z.getNombre() == "X") {
							 System.out.println("|Diesel     |    "+z.getNombre()+"     |"); 
						 }
						 if (z.getNombre() == "$") {
							 System.out.println("|BioDiesel  |    "+z.getNombre()+"     |"); 
						 }
					 }
				 }
				 else if (Locutor.getRandom() == 3 && i.getTiempo() != 0) {
					 System.out.println("Aqui vemos la disputa de los grandes premios de F-Zero... "
					 		+ "nos va a regalar una jornada repleta de buenas carreras,");
				 }
				 else if (Locutor.getRandom() == 4 && i.getTiempo() != 0) {
					 System.out.println("Se enfrentan " + columna +" buenos competidores.");
				 }
				 else if (Locutor.getRandom() == 5 && i.getTiempo() != 0) {
					 System.out.println("La velocidad actual de la nave del carril " +i.getPatente() + " es "+i.getVelocimetro().getVelocidad()) ;
				 }
				 else if (Locutor.getRandom() == 6 && i.getTiempo() != 0) {
					 System.out.println("Apostaré por "+ i.getNombre()+ " del carril " + i.getPatente());
				 }
				 else if (Locutor.getRandom() == 7 && i.getTiempo() != 0) {
					 System.out.println("El mecánico transpira de una manera!!!");
				 }
				 else if (Locutor.getRandom() == 8 && i.getTiempo() != 0) {
					 System.out.println("El mecánico no puede mas!!!");
				 }
				 else if (Locutor.getRandom() == 9 && i.getTiempo() != 0) {
					 System.out.println("A mi hijo le ensañaré a manejar a penas pueda...");
				 }
				 else if (Locutor.getRandom() == 10 && i.getTiempo() != 0) {
					 System.out.println("ojala mi hijo no maneje como la nave del carril" + i.getPatente());
				 }
			}
		}
	
	public void Reparador(int x, Nave i) {
		if (x == 1) {
			i.setDurabilidad(Mecanico.getReparar());
			i.setAceleracion((int)(Math.random() * 7) + 1);
		}
		if (x == 2) {
			i.setDurabilidad(Mecanico.getReparar());
			i.setAceleracion((int)(Math.random() * 3) + 1);
		}
		if (x == 3) {
			i.setDurabilidad(Mecanico.getReparar());
			i.setAceleracion((int)(Math.random() * 2) + 1);
		}
	}
	
	public void Durabilidad(Nave i) {
		int aux = 0;
		int deterioro;
		//System.out.print(i.getDurabilidad()+ "   ");
		if (i.getNombre() == "#") {
			deterioro = (int)(Math.random() *11) + 30;
			aux = i.getDurabilidad();
			aux = aux - deterioro;
			i.setDurabilidad(aux);
			if (i.getDurabilidad() < 0) {
				i.setDurabilidad(0);
			}	
		}
		if (i.getNombre() == "X") {
			deterioro = (int)(Math.random() *6) + 15;
			aux = i.getDurabilidad();
			aux = aux - deterioro;
			i.setDurabilidad(aux);
			if (i.getDurabilidad() < 0) {
				i.setDurabilidad(0);
			}	
		}
		if (i.getNombre() == "$") {
			deterioro = (int)(Math.random() *6) + 5;
			aux = i.getDurabilidad();
			aux = aux - deterioro;
			i.setDurabilidad(aux);	
			if (i.getDurabilidad() < 0) {
				i.setDurabilidad(0);
			}
		}
	}
		
	public static void clearScreen() {
		//System.out.print("\033[H\033[2J");
		//System.out.flush();
	}
	
	// A través de Collections se ordenan los objetos por su atributo tiempo.
	public void OrdenarTiempo() {
		int x = 0;
		String y;
		int contador = 0;
		// Se le asigna el nombre del usuario al arrayList.
		for (Nave z: NaveRobot) {
			if (contador == 0) {
				z.setNombre(nombre);
			}
			contador += 1;
		}
		contador = 0;
		Collections.sort(NaveRobot);
		// Se imprimen las posiciones.
		System.out.println("Posiciones:  "+"Segundos:  "+"Nombre:  ");
		for (Nave z: NaveRobot) {
			contador += 1;
			x = z.getTiempo();
			y = z.getNombre();
			System.out.println(contador+"°"+"             "+x+"            "+y);		
		}
	}
	
	// Función encargada que todos las naves estén apagadas.
	public int validarGanador(int inicio, int tiempo) {
		int aux = 0;
		for (Nave z: NaveRobot) {
			if (z.isInterruptor() == true) {
				aux = aux + 1;
				if (aux == participantes+1) {
					inicio = 0;
					return inicio;
				}
			}
		}
		// retorna el mismo valor de inicio, que recibió por parámetros.
		return inicio;	
	}
	
	// Función encargada de la impresión de la matriz. Además 
	public void Pista(int fila,int columna) throws InterruptedException  {
		int inicio = 1;
		int contador = 0;
		int aceleracion = 0;
		int velocidad = 0;
		int distancia = 0;
		int tiempo = 0; 
		String nombre;
		tablero = new String [fila][columna];
		while (inicio == 1) {
			tiempo = tiempo + 1;
			System.out.print("       ");
			for (Nave i: NaveRobot) {
				nombre = i.getNombre();
				velocidad = i.getVelocimetro().getVelocidad();
				distancia = i.getVelocimetro().getDistancia();
				tablero[0][contador] = nombre;
				locutor(tiempo, fila, i, contador);
				if (nombre == "#" && velocidad != fila - 1 && i.isInterruptor() == false) {
					aceleracion = i.getAceleracion();
					velocidad = i.getVelocimetro().getVelocidad();
					distancia = i.getVelocimetro().getDistancia();
					velocidad = velocidad + aceleracion;
					distancia = distancia + velocidad;
					i.getVelocimetro().setVelocidad(velocidad);
					i.getVelocimetro().setDistancia(distancia);
					Durabilidad(i);
					if (i.getDurabilidad() < 10) {
						i.setAceleracion(0);
						i.getVelocimetro().setVelocidad(0);
						Reparador(1, i);
					}
				}
				if (nombre == "X" && velocidad != fila - 1 && i.isInterruptor() == false) {
					Durabilidad(i);
					aceleracion = i.getAceleracion();
					velocidad = i.getVelocimetro().getVelocidad();
					distancia = i.getVelocimetro().getDistancia();
					velocidad = velocidad + aceleracion; 
					distancia = distancia + velocidad;
					i.getVelocimetro().setDistancia(distancia);
					i.getVelocimetro().setVelocidad(velocidad);				
					if (i.getDurabilidad() < 10) {
						i.setAceleracion(0);
						i.getVelocimetro().setVelocidad(0);
						Reparador(2, i);
					}
				}
				if (nombre == "$" && velocidad != fila - 1 && i.isInterruptor() == false) {
					Durabilidad(i);
					aceleracion = i.getAceleracion();
					velocidad = i.getVelocimetro().getVelocidad();
					distancia = i.getVelocimetro().getDistancia();
					velocidad = velocidad + aceleracion;
					distancia = distancia + velocidad;
					i.getVelocimetro().setVelocidad(velocidad);
					i.getVelocimetro().setDistancia(distancia);
					if (i.getDurabilidad() < 10) {
						i.setAceleracion(0);
						i.getVelocimetro().setVelocidad(0);
						Reparador(3, i);
					}
				}			
				// Se le da un límite, que concuerda el tamaño de la fila -1.
				if (distancia >= fila ) {
					distancia = fila - 1;
					// Se "apaga" la nave.
					i.setInterruptor(true);
				}							
				if (i.isInterruptor() == false) {
					// Si la nave no está apagada, se le sigue asignando un valor.
					i.setTiempo(tiempo);
				}
				tablero[distancia][contador] = nombre;
				contador = contador + 1;
			}
			// Se obtiene el valor que retorna ValidarGanador.
			inicio = validarGanador(inicio,tiempo);
			for(int j = 0;j < columna + 1;j ++) 
				System.out.print(" ");
				System.out.println(" ");
				for(int i = 0;i < fila; i ++) {
					System.out.print(" ");
					System.out.print("    |");
					for(int j = 0;j < columna;j ++) {
						System.out.print(" ");
						if (tablero[i][j] == null) {
							tablero [i][j] = " ";
						}
						System.out.print(tablero[i][j]);
						System.out.print(" |");
					}
					System.out.print(" ");
					for(int j = 0;j < columna + 1; j++) 
						System.out.print("    ");
						System.out.println(" ");
					}
				// Después de cada impresión de la matriz tome 2 seg. en la siguiente impresión.
				Thread.sleep(2000);
				contador = 0;
				// Se elimina el recorrido de las naves.
				for(int i = 0;i < fila; i ++) {
					for(int j = 0;j < columna;j ++) {
						tablero[i][j] = " ";
					}
				}
				if (inicio == 0) {
					System.out.println("     ---     Fin     ---");
				}
				// Al ejecutar el código como un ejecutable, se limpia la terminal.
				clearScreen();
			}
		// Una vez terminado el while se llama OrdenarTiempo.
		OrdenarTiempo();
	}
		
	// Menú del juego.
	public void MenuJuego() throws InterruptedException {
		System.out.println("Ingrese nombre de usuario: ");	
		nombre = sc.nextLine();
		while (zapatilla == true) {
			System.out.println("Seleccione el tipo de nave: ");
			System.out.println("- Nave Alfa  -Nuclear   # | 1 |");
			System.out.println("- Nave Beta  -Diesel    X | 2 |");
			System.out.println("- Nave Gamma -BioDiesel $ | 3 |");
			String option = sc.nextLine();
			fila = 30;
			columna = participantes + 1;
			if (option.equals("1")) {
				Nave NaveBots = new Nave();
				zapatilla = false;
				NaveBots.setNombre("#");
				NaveBots.setAceleracion((int)(Math.random() * 7) + 1);
				agregaMiembro(NaveBots);
				CantidadDeParticipantes();
				nCalculosFisicos();
				Pista(fila,columna);
			}
			else if (option.equals("2")) {
				Nave NaveBots = new Nave();
				zapatilla = false;
				NaveBots.setNombre("X");
				NaveBots.setAceleracion((int)(Math.random() * 3) + 1);
				agregaMiembro(NaveBots);
				CantidadDeParticipantes();
				nCalculosFisicos();
				Pista(fila,columna);
			}		
			else if (option.equals("3")) {
				Nave NaveBots = new Nave();
				zapatilla = false;
				NaveBots.setNombre("$");
				NaveBots.setAceleracion((int)(Math.random() * 2) + 1);
				agregaMiembro(NaveBots);
				CantidadDeParticipantes();
				nCalculosFisicos();
				Pista(fila,columna);
			}
			else {
				System.out.println("Ingrese un valor válido.");
			}
		}
	}
	// Se crean nCantidad de participantes.
	public void CantidadDeParticipantes() {
		for (int i = 1; i <= this.participantes; i++) {
			int tipoNave = (int)(Math.random() *3) + 1;
			// Se crean naves alfa.
			if (tipoNave == 1) {
				Nave NaveBots = new Nave();
				NaveBots.setAceleracion((int)(Math.random() * 7) + 1);
				NaveBots.setNombre("#");
				agregaMiembro(NaveBots);
			}
			// Se crean naves beta.
			if (tipoNave == 2) {
				Nave NaveBots = new Nave();
				NaveBots.setAceleracion((int)(Math.random() * 3) + 1);
				NaveBots.setNombre("X");
				agregaMiembro(NaveBots);
			}
			// Se crean naves gamma.
			if (tipoNave == 3) {
				Nave NaveBots = new Nave();
				NaveBots.setAceleracion((int)(Math.random() * 2) + 1);
				NaveBots.setNombre("$");
				agregaMiembro(NaveBots);
			}
		}	
	}
	
	// Se le asigna a cada objeto un valor en su aceleración, velocidad y distancia.
	public void nCalculosFisicos() {
		int contador = 1;
		for (Nave i: NaveRobot) {
			i.setPatente(contador);
			i.getVelocimetro().setVelocidad(0);
			i.getVelocimetro().setDistancia(0);
			contador += 1;
		}
	}
	
	// Se agregan los n objetos al arrayList NaveRobot.
	public void agregaMiembro(Nave naveBots) {	
		NaveRobot.add(naveBots);
	}
}