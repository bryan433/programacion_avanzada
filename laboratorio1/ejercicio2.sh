#!/bin/bash -x

function directorios(){
    DIRECTORIO1=1
    DIRECTORIO2=2
    DIRECTORIO3=3

    if [[ ! -d "$DIRECTORIO1" && ! -d "$DIRECTORIO2" && ! -d "$DIRECTORIO3" ]]; then
        echo "El directorio no existe"
        echo "se creara el directorio"
        mkdir $DIRECTORIO1
        mkdir $DIRECTORIO2
        mkdir $DIRECTORIO3 
    else
        echo "El directorio ya esta creado con ese nombre" 
    fi
    echo "hola"
    for i in `ls`; do
		letra=`echo $i | awk '{print substr($i,1,1)}'`
		mv $i $letra/    
    done   
}

function archivo_temporal(){
    ARCHIVO1="1t4v_docking.mol2"
    ARCHIVO2="1t4v_referencia.mol2"
    ARCHIVO3="2r2m_docking.mol2"
    ARCHIVO4="2r2m_referencia.mol2"
    ARCHIVO5="3ldx_docking.mol2"
    ARCHIVO6="3ldx_docking_new.mol2"
    ARCHIVO7="3ldx_referencia.mol2"
    ARCHIVO8="3ldx_referencia_new.mol2"

    if [[ ! -f "$ARCHIVO1" && ! -f "$ARCHIVO2" && ! -f "$ARCHIVO3" && ! -f "$ARCHIVO4" && ! -f "$ARCHIVO5" && ! -f "$ARCHIVO6" && ! -f "$ARCHIVO7" && ! -f "$ARCHIVO8" ]]; then
        echo
        echo "El archivos temporales no existen"
        echo "se creara el directorio"
        touch $ARCHIVO1
        touch $ARCHIVO2
        touch $ARCHIVO3
        touch $ARCHIVO4
        touch $ARCHIVO5
        touch $ARCHIVO6
        touch $ARCHIVO7
        touch $ARCHIVO8
        directorios
        exit 1 
        else
            echo
            echo "El fichero ya se encuentra creado"
    fi
}




cd

A=$1

if [[ ! -d "$A" ]]; then
    echo "El archivo $A no existe"
    echo "se creara el directorio"
    mkdir $A
    cd $A/
    echo "$PWD"
    archivo_temporal
    exit 1 
    else
        echo "El directorio ya esta creado con ese nombre"
        echo
        echo "¿Desea cambiar el nombre de su directorio? (Presione s para aceptar o cualquier letra para rechazar)"	
        read letra
        if  [[ $letra == 's' ]]; then 
            echo "Debe ingresar un nuevo nombre para el directorio: "
            read B
            mv $A $B
            echo "se cambio el nombre del directorio"
        fi
fi
