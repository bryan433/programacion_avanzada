#!/bin/bash 

PARAMETRO=$1
DIA=`date +%d`
MES=`date +%m` 
ANIO=`date +%Y`

if [[ $PARAMETRO == "-s" ]] || [[ $PARAMETRO == "--short" ]]; then
    date +%Y.%m.%d
fi

if [[ $PARAMETRO == "-l" ]] || [[ $PARAMETRO == "--long" ]]; then
    echo "Hoy es el dia $dia, del mes $MES y del año $ANIO" 
fi

if [[ $PARAMETRO == "" ]]; then
    cal
fi

if [[ $#>1 ]]; then
    echo "solo se admite un parametro"
    exit 1
fi

if [[ $PARAMETRO != "-s" ]] || [[ $PARAMETRO != "--short" ]] || [[ $PARAMETRO != "-l" ]] || [[ $PARAMETRO != "--long" ]]; then 
    echo "Opcion incorrecta, solo se acepta el parametro -s(--short) o -l(--long)"
    exit 1
fi
 
