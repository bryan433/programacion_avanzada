BEGIN{
    print "digraph G {"
    print "rankdir=LR;"
    print "size="200""
    primero = 1 
}
{
    filas[NR]
    if (primero == 1){
        p=$3$4$5
        cambio_aminoacido=$3$4$5
        d="\""
        print "node [shape=circle]"
        print cambio_aminoacido " [label = " d$3d "] "
        print p "->" $6 "[label = "d"cadena"" "$4d"];"
        primero = 0
    }
    cambio_varialbe=$2$1
    cambio_aminoacido=$3$4$5
    d="\""
    print "node [shape=circle]"
    print cambio_varialbe " [label = " d$2d "] "
    print cambio_aminoacido " [label = " d$3d "] "
    print cambio_varialbe "->" cambio_aminoacido
    auxiliar = $3$4$5
    if (p != auxiliar){
    print "node [shape=box]"
    print p "->" auxiliar "[label = "d"cadena"" "$4d"];"
    print auxiliar "->" $6 "[label = "d"cadena"" "$4d"];" 
    p = $3$4$5 
    }
}        

END{
    
    print "}"    
}


