#!/bin/bash -x

NOMBRE_PROTEINA=$1

function recorrer_fichero(){
	if [[ $NOMBRE_PROTEINA = "" ]]; then
	    echo "Debe ingresar alguna proteina como parametro,"
	    echo "intentelo nuevamente ejecutando el programa"
	    exit 1
	else     
        while read linea; do
	        echo $linea
        done < bd-pdb.txt | grep \"$NOMBRE_PROTEINA\" | grep \"Protein\"
    fi	
}

function verificar_proteina(){
    recorrer_fichero    
    m=`cat bd-pdb.txt | grep \"$NOMBRE_PROTEINA\" | awk {"print $4"}`
    n=`cat bd-pdb.txt | grep \"$NOMBRE_PROTEINA\" | grep \"Protein\"`
    if [[ $n = $m ]]; then
        echo
        echo "La proteina que buscaba, se encuentra en nuestra base de datos"
        echo
        echo "--------------------------------------------------------------------------------"
        comprobar_conexion
    else
        echo
        echo "La proteina no esta en la base de datos, o no es una proteina"
        echo 
    fi    
}

function comprobar_conexion(){
    if ping -c 1 google.com; then
       echo "Usted tiene conexion de internet, podremos descargar el archivo"
       descargar_proteina
    else
        echo "Usted no tiene conexion de internet, conecte su PC, para descargar el archivo" 
    fi
}

function descargar_proteina(){
    if [[ ! -f $NOMBRE_PROTEINA.pdb ]]; then
        echo 
        echo "--------------------------------------------------------------------------------"
        echo "No existe el archivo, lo descargaremos"
        echo 
        wget https://files.rcsb.org/download/$NOMBRE_PROTEINA.pdb 
        cat $NOMBRE_PROTEINA.pdb | grep ^ATOM | awk 'length($3) > 3{$3 = substr($3,1,3) "  " substr($3,5,3)}length($4) > 3{$4 = substr($4,2,3)}{print $0}' > $NOMBRE_PROTEINA.txt
        cat $NOMBRE_PROTEINA.pdb | grep ^HETATM | awk 'length($1) > 6{$1 = substr($1,1,6) "  " substr($1,7,11)}{print $0}' > HETATM.txt
        cat HETATM.txt | grep ^HETATM | awk 'length($5)>1{$5 = substr($5,1,1)" "substr($5,2,6)}{print $0}' > filtro.txt
        cat filtro.txt | grep ^HETATM | awk '$4 != "HOH"{print $0}' >filtro_HETATM.txt 
        cat filtro_HETATM.txt | awk -f punto_geometrico.awk
        echo "ingrese el radio que desea su grafico: "
        read distancia
        for i in $(ls | grep ^"Z"); do
            PG_x=`awk '{print $5}' $i`
            PG_y=`awk '{print $6}' $i`
            PG_z=`awk '{print $7}' $i`
            nombre=`awk '{print $3}' $i`
            awk -v X_cg=$PG_x -v Y_cg=$PG_y -v Z_cg=$PG_z -v D=$distancia -v nombre=$nombre -f distancia.awk $NOMBRE_PROTEINA.txt > "ROLA"$i
            awk -f graficar.awk "ROLA"$i >> "grafico"$i.dot
            dot -Tps -o $NOMBRE_PROTEINA_$i.ps "grafico"$i.dot
            rm -f "grafico"$i.dot
            rm -f "Z"$i
            rm -f "ROLA"$i
        done 
       
        #~ echo
        #~ echo "--------------------------------------------------------------------------------"
        #~ echo "Vamos a graficar su proteina: "
        #~ dot -Tps -o $NOMBRE_PROTEINA.ps $NOMBRE_PROTEINA.dot
        #~ echo "El grafico se encuentra en la carpeta del proyecto"
        echo "¿Desea borrar los archivos temporales?(Si/s o cualquier letra para no borrar)"
        read opcion
        if [[ opcion -eq 's' ]] || [[ opcion -eq 'S' ]]; then
            rm -f HETATM.txt
            rm -f $NOMBRE_PROTEINA.pdb
            rm -f filtro.txt
            rm -f filtro_HETATM.txt
        else
            echo "No se ha borrado ningun fichero"    
        fi
    else
        echo "ya esta descargado el archivo"
        cat $NOMBRE_PROTEINA.txt
        fi	
}


verificar_proteina
